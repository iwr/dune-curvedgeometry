dune_add_test(SOURCES test-lfecache.cc
              LINK_LIBRARIES ${DUNE_LIBS}
              CMAKE_GUARD dune-localfunctions_FOUND)