// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
#include "config.h"
#endif

#include <dune/common/hybridutilities.hh>
#include <dune/common/rangeutilities.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/geometry/type.hh>
#include <dune/localfunctions/lagrange/lfecache.hh>

template <class FiniteElementCache>
int test (FiniteElementCache& cache, Dune::GeometryType type)
{
  using FiniteElement = typename FiniteElementCache::FiniteElementType;
  const FiniteElement& finiteElement = cache.get(type);

  if (!(finiteElement.type() == type))
    DUNE_THROW(Dune::Exception, "Wrong GeometryType in constructed local finite-element");

  return finiteElement.localBasis().order();
}

int main()
{
  using namespace Dune;
  const int max_dim = 3;
  const unsigned int max_order = 4;

  TestSuite testSuite;
  Hybrid::forEach(StaticIntegralRange<int,max_dim+1,1>{},[&](auto dim)
  {
    // runtime order parameter
    for (unsigned int k = 1; k <= max_order; ++k) {
      using FiniteElementCache = LagrangeLFECache<double, double, dim>;
      FiniteElementCache cache{k};
      testSuite.check(test(cache, Dune::GeometryTypes::simplex(dim)) == k);
      testSuite.check(test(cache, Dune::GeometryTypes::cube(dim)) == k);
    }
  });

  return testSuite.exit();
}
