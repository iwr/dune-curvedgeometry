#ifndef DUNE_LOCALFUNCTIONS_LAGRANGE_LFECACHE_HH
#define DUNE_LOCALFUNCTIONS_LAGRANGE_LFECACHE_HH

#if !HAVE_DUNE_LOCALFUNCTIONS
#error "LagrangeLFECache needs dune-localfunctions"
#endif

#include <map>

#include <dune/geometry/type.hh>
#include <dune/localfunctions/lagrange.hh>
#include <dune/localfunctions/lagrange/equidistantpoints.hh>

namespace Dune {

/// \brief Cache for the generic \ref LagrangeLocalFiniteElement
/**
 * \tparam Domain  Field-type of the domain of the local basis functions
 * \tparam Range   Field-type of the range of the local basis functions
 * \tparam dim     Dimension of the local domain
 * \tparam LP      Lagrange point-set template <class Field, unsigned int dim>, default: \ref EquidistantPointSet
 **/
template <class Domain, class Range, int dim,
          template <class,unsigned int> class LP = EquidistantPointSet>
class LagrangeLFECache
{
public:
  using FiniteElementType = LagrangeLocalFiniteElement<LP, dim, Domain, Range>;

  LagrangeLFECache (unsigned int order = 0)
    : order_(order)
  {}

  const FiniteElementType& get (GeometryType type)
  {
    auto it = data_.find(type);
    if (it == data_.end())
      it = data_.emplace(type,FiniteElementType(type,order_)).first;
    return it->second;
  }

private:
  unsigned int order_ = 0;
  std::map<GeometryType, FiniteElementType> data_ = {};
};

} // end namespace Dune

#endif // DUNE_LOCALFUNCTIONS_LAGRANGE_LFECACHE_HH
