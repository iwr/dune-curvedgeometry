#install headers
install(FILES
  lfecache.hh
  DESTINATION ${CMAKE_INSTALL_INCLUDEDIR}/dune/localfunctions/lagrange)

add_subdirectory(test)