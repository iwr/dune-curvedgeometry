#ifndef DUNE_CURVEDGEOMETRY_UTILITY_HH
#define DUNE_CURVEDGEOMETRY_UTILITY_HH

#include <dune/common/diagonalmatrix.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>

namespace Dune {
namespace Impl {

// matrix-matrix multiplication A*B^t
template <class K, int n, int m, int l>
FieldMatrix<K,n,m> ABt (const FieldMatrix<K,n,l>& A, const FieldMatrix<K,m,l>& B)
{
  FieldMatrix<K,n,m> ABt;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j) {
      ABt[i][j] = 0;
      for (int k = 0; k < l; ++k)
        ABt[i][j] += A[i][k] * B[j][k];
    }

  return ABt;
}

// matrix-matrix multiplication A*B^t where A is a diagonal matrix
template <class K, int n, int m>
FieldMatrix<K,n,m> ABt (const DiagonalMatrix<K,n>& A, const FieldMatrix<K,m,n>& B)
{
  FieldMatrix<K,n,m> ABt;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j) {
      ABt[i][j] = A[i][i] * B[j][i];
    }

  return ABt;
}


// matrix-matrix multiplication A*B
template <class K, int n, int m, int l>
FieldMatrix<K,n,m> AB (const FieldMatrix<K,n,l>& A, const FieldMatrix<K,l,m>& B)
{
  FieldMatrix<K,n,m> AB;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j) {
      AB[i][j] = 0;
      for (int k = 0; k < l; ++k)
        AB[i][j] += A[i][k] * B[k][j];
    }

  return AB;
}

// matrix-matrix multiplication A*B
template <class K, int n, int m>
FieldMatrix<K,n,m> AB (const FieldMatrix<K,n,m>& A, const DiagonalMatrix<K,m>& B)
{
  FieldMatrix<K,n,m> AB;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      AB[i][j] = A[i][j] * B[j][j];

  return AB;
}

// matrix-matrix multiplication A*B where A is a diagonal matrix
template <class K, int n, int m>
FieldMatrix<K,n,m> AB (const DiagonalMatrix<K,n>& A, const FieldMatrix<K,n,m>& B)
{
  FieldMatrix<K,n,m> AB;
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      AB[i][j] = A[i][i] * B[i][j];

  return AB;
}

// matrix-matrix multiplication A*B where both matrices are diagonal matrices
template <class K, int n>
DiagonalMatrix<K,n> AB (const DiagonalMatrix<K,n>& A, const DiagonalMatrix<K,n>& B)
{
  DiagonalMatrix<K,n> AB;
  for (int i = 0; i < n; ++i)
    AB[i][i] = A[i][i] * B[i][i];

  return AB;
}

// Let a and b be two column vectors then res += a^T*b
template <class T, int n, int m>
void outerProductAccumulate (const FieldVector<T,n>& a, const FieldVector<T,m>& b, FieldMatrix<T,n,m>& res)
{
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      res[i][j] += a[i] * b[j];
}

// Let a and b be two column vectors then res += a^T*b
template <class T, int n, int m>
void outerProductAccumulate (const FieldMatrix<T,n,1>& a, const FieldVector<T,m>& b, FieldMatrix<T,n,m>& res)
{
  for (int i = 0; i < n; ++i)
    for (int j = 0; j < m; ++j)
      res[i][j] += a[i][0] * b[j];
}

// Let a be a row vector and b be a column vector then res += a*b
template <class T, int n, int m,
  std::enable_if_t<(n > 1), int> = 0>
void outerProductAccumulate (const FieldMatrix<T,1,n>& a, const FieldVector<T,m>& b, FieldMatrix<T,n,m>& res)
{
  outerProductAccumulate(a[0], b, res);
}

} // end namespace Impl
} // end namespace Dune

#endif // DUNE_CURVEDGEOMETRY_UTILITY_HH
