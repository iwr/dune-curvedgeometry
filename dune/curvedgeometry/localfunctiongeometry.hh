#ifndef DUNE_CURVEDGEOMETRY_LOCALFUNCTIONGEOMETRY_HH
#define DUNE_CURVEDGEOMETRY_LOCALFUNCTIONGEOMETRY_HH

#include <cassert>
#include <limits>
#include <optional>
#include <type_traits>
#include <vector>

#include <dune/common/exceptions.hh>
#include <dune/common/fmatrix.hh>
#include <dune/common/fvector.hh>
#include <dune/geometry/affinegeometry.hh>
#include <dune/geometry/multilineargeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/geometry/referenceelements.hh>
#include <dune/geometry/type.hh>

#include "localgeometry.hh"
#include "utility.hh"

namespace Dune {

/// \brief Default traits class for LocalFunctionGeometryTraits
/**
 *  The LocalFunctionGeometry allows tweaking
 *  some implementation details through a traits class.
 *
 *  This structure provides the default values.
 *
 *  \tparam  ct  coordinate type
 **/
template <class ct>
struct LocalFunctionGeometryTraits
{
  /// \brief helper structure containing some matrix routines. See affinegeometry.hh
  using MatrixHelper = Impl::FieldMatrixHelper<ct>;

  /// \brief tolerance to numerical algorithms
  static ct tolerance () { return ct(16) * std::numeric_limits<ct>::epsilon(); }

  /// \brief maximal number of Newton iteration in `geometry.local(global)`
  static int maxIteration () { return 100; }

  /// \brief Geometry is associated to just one GeometryType
  template <int dim>
  struct hasSingleGeometryType
  {
    static const bool v = false;
    static const unsigned int topologyId = ~0u; //< optionally, the topologyId of the single GeometryType
  };
};


/// \brief Geometry parametrized by a LocalFunction and a LocalGeometry
/**
 * \tparam LF  Local function implementing the concept Functions::Concept::LocalFunction
 * \tparam LG  A local geometry type defining the local coordinates and the input coordinates
 *             for the local function. See \ref DefaultLocalGeometry
 * \tparam TraitsType  Parameters of the geometry, see \ref LocalFunctionGeometryTraits
 *
 *  The requirements on the traits are documented along with their default,
 *  LocalFunctionGeometryTraits.
 **/
template <class LF, class LG,
          class TraitsType = LocalFunctionGeometryTraits<typename LG::LocalCoordinate::value_type>>
class LocalFunctionGeometry
{
public:
  /// type of local coordinates
  using LocalCoordinate = typename LG::LocalCoordinate;

  /// type of global coordinates
  using GlobalCoordinate = std::invoke_result_t<LF,typename LG::GlobalCoordinate>;

  /// coordinate type
  using ctype = typename LocalCoordinate::value_type;

  /// geometry dimension
  static const int mydimension = LocalCoordinate::size();

  /// coordinate dimension
  static const int coorddimension = GlobalCoordinate::size();

  /// type of volume
  using Volume = ctype;

  /// type of jacobian (transposed)
  using JacobianTransposed = FieldMatrix<ctype, mydimension, coorddimension>;
  using Jacobian = FieldMatrix<ctype, coorddimension, mydimension>;

  /// type of jacobian inverse (transposed)
  using JacobianInverseTransposed = FieldMatrix<ctype, coorddimension, mydimension>;
  using JacobianInverse = FieldMatrix<ctype, mydimension, coorddimension>;

  /// type of the extended Weingarten map
  using NormalGradient = FieldMatrix<ctype, coorddimension, coorddimension>;

public:
  /// type of reference element
  using ReferenceElements = Dune::ReferenceElements<ctype, mydimension>;
  using ReferenceElement = typename ReferenceElements::ReferenceElement;

  /// Parametrization of the geometry
  using Traits = TraitsType;

protected:
  using MatrixHelper = typename Traits::MatrixHelper;
  static const bool hasSingleGeometryType
    = Traits::template hasSingleGeometryType<mydimension>::v;
  static const bool isFlatAffine
    = hasSingleGeometryType && ((Traits::template hasSingleGeometryType<mydimension>::topologyId) >> 1 == 0);

  /// type of coordinate transformation for subEntities to codim=0 entities
  using LocalGeometry = LG;

  /// type of the localFunction representation the geometry parametrization
  using LocalFunction = LF;

  /// type of the LocalFunction representing the derivative
  using DerivativeLocalFunction = std::decay_t<decltype(derivative(std::declval<LF>()))>;

public:
  /// \brief Constructor from localFunction to parametrize the geometry
  /**
   *  \param[in]  refElement     reference element for the geometry
   *  \param[in]  localFunction  localFunction for the parametrization of the geometry
   *                             (stored by value)
   *  \param[in]  localGeometry  local geometry for local coordinate transformation
   *
   **/
  template <class LF_, class LG_>
  LocalFunctionGeometry (const ReferenceElement& refElement, LF_&& localFunction, LG_&& localGeometry)
    : refElement_(refElement)
    , localFunction_(std::forward<LF_>(localFunction))
    , localGeometry_(std::forward<LG_>(localGeometry))
  {}

  /// \brief Constructor, forwarding to the other constructor that take a reference-element
  /**
   *  \param[in]  gt             geometry type
   *  \param[in]  localFunction  localFunction for the parametrization of the geometry
   *                             (stored by value)
   *  \param[in]  localGeometry  local geometry for local coordinate transformation
   **/
  template <class LF_, class LG_>
  LocalFunctionGeometry (GeometryType gt, LF_&& localFunction, LG_&& localGeometry)
    : LocalFunctionGeometry(ReferenceElements::general(gt),
                            std::forward<LF_>(localFunction),
                            std::forward<LG_>(localGeometry))
  {}

  /// \brief Constructor, forwarding to the other constructors, with LocalGeometry is
  /// DefaultLocalGeometry.
  /**
   *  \param[in]  refGeo         geometry type or reference element
   *  \param[in]  localFunction  localFunction for the parametrization of the geometry
   *                             (stored by value)
   **/
  template <class RefGeo, class LF_, class LG_ = LG,
    std::enable_if_t<std::is_same_v<LG_, DefaultLocalGeometry<ctype,mydimension>>, bool> = true>
  LocalFunctionGeometry (RefGeo&& refGeo, LF_&& localFunction)
    : LocalFunctionGeometry(std::forward<RefGeo>(refGeo),
                            std::forward<LF_>(localFunction),
                            LG_{})
  {}

  LocalFunctionGeometry (const LocalFunctionGeometry&)
    noexcept(std::is_nothrow_copy_constructible_v<LF>) = default;

  LocalFunctionGeometry (LocalFunctionGeometry&&)
    noexcept(std::is_nothrow_move_constructible_v<LF>) = default;

  /// \brief copy-assignment operator
  LocalFunctionGeometry& operator= (const LocalFunctionGeometry& that)
    noexcept(std::is_nothrow_copy_constructible_v<LF>)
  {
    refElement_ = that.refElement_;
    localFunction_.reset();
    if (that.localFunction_)
      localFunction_.emplace(*that.localFunction_);
    localGeometry_ = that.localGeometry_;
    return *this;
  }

  /// \brief move-assignment operator
  LocalFunctionGeometry& operator= (LocalFunctionGeometry&& that)
    noexcept(std::is_nothrow_copy_constructible_v<LF>)
  {
    refElement_ = std::move(that.refElement_);
    localFunction_.reset();
    if (that.localFunction_)
      localFunction_.emplace(*that.localFunction_);
    localGeometry_ = std::move(that.localGeometry_);
    return *this;
  }

  /// \brief Is this mapping affine? No! Since we do not know anything about the mapping.
  bool affine () const
  {
    return false;
  }

  /// \brief Obtain the element type from the reference element
  GeometryType type () const
  {
    return refElement_.type();
  }

  /// \brief Obtain number of corners of the corresponding reference element
  int corners () const
  {
    return refElement_.size(mydimension);
  }

  /// \brief Obtain coordinates of the i-th corner
  GlobalCoordinate corner (int i) const
  {
    assert( (i >= 0) && (i < corners()) );
    return global(refElement_.position(i, mydimension));
  }

  /// \brief Obtain the centroid of the mapping's image
  GlobalCoordinate center () const
  {
    return global(refElement_.position(0, 0));
  }

  /// \brief Evaluate the coordinate mapping
  /**
   *  Evaluate the local function in local coordinates
   *
   *  \param[in] local  local coordinates
   *  \returns          corresponding global coordinate
   **/
  GlobalCoordinate global (const LocalCoordinate& local) const
  {
    return localFunction()(localGeometry().global(local));
  }

  /// \brief Evaluate the inverse coordinate mapping
  /**
   *  \param[in] globalCoord  global coordinate to map
   *  \return                 corresponding local coordinate
   *
   *  \throws in case of an error indicating that the local coordinate can not be obtained,
   *          an exception is thrown. See \ref checkedLocal for a variant that returns
   *          an optional instead.
   *
   *  \note For given global coordinate `y` the returned local coordinate `x` that minimizes
   *  the following function over the local coordinate space spanned by the reference element.
   *  \code
   *  (global( x ) - y).two_norm()
   *  \endcode
   **/
  LocalCoordinate local (const GlobalCoordinate& globalCoord) const
  {
    auto localCoord = checkedLocal(globalCoord);
    if (!localCoord)
      DUNE_THROW(Dune::Exception,
        "local coordinate can not be recovered from global coordinate " << globalCoord);

    return *localCoord;
  }

  /// \brief Evaluate the inverse coordinate mapping
  /**
   *  \param[in] globalCoord  global coordinate to map
   *  \return                 corresponding local coordinate or nothing, in case the local
   *                          coordinate could not be calculated. The return value is wrapped
   *                          in an optional.
   **/
  std::optional<LocalCoordinate> checkedLocal (const GlobalCoordinate& globalCoord) const
  {
    LocalCoordinate x = flatGeometry().local(globalCoord);
    LocalCoordinate dx;

    for (int i = 0; i < Traits::maxIteration(); ++i)
    {
      // Newton's method: DF^n dx^n = F^n, x^{n+1} -= dx^n
      const GlobalCoordinate dglobal = global(x) - globalCoord;
      const bool invertible = MatrixHelper::xTRightInvA(jacobianTransposed(x), dglobal, dx);

      // break if jacobian is not invertible
      if (!invertible)
        return std::nullopt;

      // update x with correction
      x -= dx;

      // break if tolerance is reached.
      if (dx.two_norm2() < Traits::tolerance())
        return x;
    }

    if (dx.two_norm2() > Traits::tolerance())
      return std::nullopt;

    return x;
  }

  /// \brief Construct a normal vector of the curved element evaluated at
  /// a given local coordinate
  /**
   * \note Implemented for codim=1 entities only, i.e. edges in 2D and faces in 3D
   **/
  GlobalCoordinate normal (const LocalCoordinate& local) const
  {
    GlobalCoordinate n = normalDirection(local);
    return n / n.two_norm();
  }

  /// \brief Construct a normal direction (not normalized) of the curved element
  /// evaluated at a given local coordinate
  /**
   * \note Implemented for codim=1 entities only, i.e. edges in 2D and faces in 3D
   **/
  GlobalCoordinate normalDirection (const LocalCoordinate& local) const
  {
    assert(coorddimension == mydimension+1);
    return [&]() {
      if constexpr ((mydimension == 1) && (coorddimension == 2)) { return normalDirection1D(local); }
      else if constexpr ((mydimension == 2) && (coorddimension == 3)) { return normalDirection2D(local); }
      else return GlobalCoordinate(0);
    }();
  }

  NormalGradient normalGradient (const LocalCoordinate& local) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "The normalGradient is not implemented without providing a local finite-element. "
      "See the overloads of the function for alternatives.");
    return NormalGradient(0);
  }

  NormalGradient normalGradientImpl (const LocalCoordinate& local, const JacobianInverseTransposed& jiT) const
  {
    DUNE_THROW(Dune::NotImplemented,
      "The normalGradient is not implemented without providing a local finite-element. "
      "See the overloads of the function for alternatives.");
    return NormalGradient(0);
  }

  /// \brief Construct the surface gradient (extended Weingarten map) of the normal vector field
  /**
   * First, interpolate the normal vector field into a local Lagrange basis, then take the
   * derivative if this interpolated field, normalized it and project it into the tangential
   * plane.
   *
   * \param local    The local coordinate where to evaluate the normal-vector gradient
   * \param localFE  Local finite-element of order large enough, used to interpolate the normal.
   **/
  template <class LFE>
  NormalGradient normalGradient (const LocalCoordinate& local, LFE const& localFE) const
  {
    return normalGradientImpl(local, jacobianInverseTransposed(local), localFE);
  }

  /// \brief Construct the surface gradient (extended Weingarten map) of the normal vector field
  /**
   * See \ref normalGradient() but with additional parameter.
   *
   * \param jiT   Evaluation of the JacobianInverseTransposed at the local coordinate `local`.
   *              This can be passed, if already computed elsewhere.
   **/
  template <class LFE>
  NormalGradient normalGradientImpl (const LocalCoordinate& local, const JacobianInverseTransposed& jiT,
                                     LFE const& localFE) const
  {
    // construct a local finite-element type of the given order on the element
    using LocalBasisTraits = typename LFE::Traits::LocalBasisType::Traits;

    if (nCoefficients_.empty()) {
      // create local discrete function of normal vectors by interpolation of the geometry normal
      localFE.localInterpolation().interpolate(
        [&](const LocalCoordinate& l) { return this->normalDirection(l); }, nCoefficients_);
    }

    // Interpolated normal vector evaluated at local coordinate
    thread_local std::vector<typename LocalBasisTraits::RangeType> nShapeValues;
    localFE.localBasis().evaluateFunction(local, nShapeValues);
    GlobalCoordinate nh(0);
    for (std::size_t j = 0; j < nShapeValues.size(); ++j)
      nh.axpy(nShapeValues[j], nCoefficients_[j]);
    auto nh_nrm = nh.two_norm();
    nh /= nh_nrm;

    // P = I - n x n
    NormalGradient Ph;
    for (int r = 0; r < coorddimension; ++r)
      for (int s = 0; s < coorddimension; ++s)
        Ph[r][s] = (r == s ? 1 : 0) - nh[r]*nh[s];

    // Compute the shape function gradients on the real element
    thread_local std::vector<typename LocalBasisTraits::JacobianType> nShapeGradients;
    localFE.localBasis().evaluateJacobian(local, nShapeGradients);
    nGradients_.resize(nShapeGradients.size());
    for (std::size_t j = 0; j < nGradients_.size(); ++j)
      jiT.mv(nShapeGradients[j][0], nGradients_[j]);

    // Normal gradient evaluated at local coordinate
    NormalGradient H(0);
    for (std::size_t j = 0; j < nGradients_.size(); ++j)
      for (int r = 0; r < coorddimension; ++r)
        for (int s = 0; s < coorddimension; ++s)
          H[r][s] += nGradients_[j][s] * nCoefficients_[j][r];
    H /= nh_nrm;
    H.leftmultiply(Ph);
    H.rightmultiply(Ph);

    return H;
  }

  ///  \brief Obtain the integration element
  /**
   *  If the Jacobian of the mapping is denoted by $J(x)$, the integration
   *  integration element \f$\mu(x)\f$ is given by
   *  \f[ \mu(x) = \sqrt{|\det (J^T(x) J(x))|}.\f]
   *
   *  \param[in]  local  local coordinate to evaluate the integration element in
   *
   *  \returns the integration element \f$\mu(x)\f$.
   **/
  ctype integrationElement (const LocalCoordinate& local) const
  {
    return MatrixHelper::sqrtDetAAT(jacobianTransposed(local));
  }

  /// \brief Obtain the volume of the mapping's image
  /**
   * Calculates the volume of the entity by numerical integration. Since the polynomial order of the
   * Volume element is not known, iteratively compute numerical integrals with increasing order
   * of the quadrature rules, until tolerance is reached.
   **/
  Volume volume () const
  {
    using std::abs;
    Volume vol0 = volume(QuadratureRules<ctype, mydimension>::rule(type(), 1));
    for (int p = 2; p < 10; ++p) {
      Volume vol1 = volume(QuadratureRules<ctype, mydimension>::rule(type(), p));
      if (abs(vol1 - vol0) < Traits::tolerance())
        return vol1;

      vol0 = vol1;
    }
    return vol0;
  }

  /// \brief Obtain the volume of the mapping's image by given quadrature rules
  template <class QuadRule>
  Volume volume (const QuadRule& quadRule) const
  {
    Volume vol(0);
    for (const auto& qp : quadRule)
      vol += integrationElement(qp.position()) * qp.weight();
    return vol;
  }

  /// \brief Obtain the transposed of the Jacobian
  /**
   *  \param[in]  local  local coordinate to evaluate Jacobian in
   *  \returns           the matrix corresponding to the transposed of the Jacobian
   **/
  JacobianTransposed jacobianTransposed (const LocalCoordinate& local) const
  {
    if (!derivativeLocalFunction_) {
      derivativeLocalFunction_.emplace(derivative(localFunction()));
      derivativeLocalFunction_->bind(localFunction().localContext());
    }

    // coordinate in the localContext of the localFunction
    auto&& elementLocal = localGeometry().global(local);

    auto&& jLocal = localGeometry().jacobianTransposed(local);
    auto&& jGlobal = hostGeometry().jacobianTransposed(elementLocal);
    auto&& jacobian = (*derivativeLocalFunction_)(elementLocal);
    return Impl::AB(jLocal,Impl::ABt(jGlobal,jacobian));
  }

  Jacobian jacobian (const LocalCoordinate& local) const
  {
    return jacobianTransposed(local).transposed();
  }

  /// \brief obtain the transposed of the Jacobian's inverse
  /**
   *  The Jacobian's inverse is defined as a pseudo-inverse. If we denote
   *  the Jacobian by \f$J(x)\f$, the following condition holds:
   *  \f[ J^{-1}(x) J(x) = I. \f]
   **/
  JacobianInverseTransposed jacobianInverseTransposed (const LocalCoordinate& local) const
  {
    JacobianInverseTransposed out;
    MatrixHelper::rightInvA(jacobianTransposed(local), out);
    return out;
  }

  JacobianInverse jacobianInverse (const LocalCoordinate& local) const
  {
    return jacobianInverseTransposed(local).transposed();
  }

  /// \brief obtain the reference-element related to this geometry
  friend ReferenceElement referenceElement (const LocalFunctionGeometry& geometry)
  {
    return geometry.refElement();
  }

protected:
  // the internal stored reference element
  const ReferenceElement& refElement () const
  {
    return refElement_;
  }

  // normal vector to an edge line-element
  GlobalCoordinate normalDirection1D (const LocalCoordinate& local) const
  {
    auto J = jacobianTransposed(local);
    return GlobalCoordinate{
       J[0][1],
      -J[0][0]};
  }

  // normal vector to a triangle or quad face element
  GlobalCoordinate normalDirection2D (const LocalCoordinate& local) const
  {
    auto J = jacobianTransposed(local);
    return GlobalCoordinate{
      J[0][1] * J[1][2] - J[0][2] * J[1][1],
      J[0][2] * J[1][0] - J[0][0] * J[1][2],
      J[0][0] * J[1][1] - J[0][1] * J[1][0]};
  }

private:
  // internal reference to the stored localfunction
  const LocalFunction& localFunction () const
  {
    assert(!!localFunction_);
    return *localFunction_;
  }

  // internal reference to the stored localgeometry
  const LocalGeometry& localGeometry () const
  {
    return localGeometry_;
  }

  // Construct a flat geometry from corner vertices
  using FlatGeometry = std::conditional_t<isFlatAffine,
    AffineGeometry<ctype, mydimension, coorddimension>,
    MultiLinearGeometry<ctype, mydimension, coorddimension>>;
  const FlatGeometry& flatGeometry () const
  {
    if (!flatGeometry_) {
      std::vector<GlobalCoordinate> corners;
      corners.reserve(refElement_.size(mydimension));
      for (int i = 0; i < refElement_.size(mydimension); ++i)
        corners.push_back(global(refElement_.position(i, mydimension)));

      flatGeometry_.emplace(refElement_, std::move(corners));
    }

    return *flatGeometry_;
  }

  // Return the geometry of the element the localFunction is bound to
  using HostGeometry = std::decay_t<decltype(std::declval<LF>().localContext().geometry())>;
  const HostGeometry& hostGeometry () const
  {
    if (!hostGeometry_)
      hostGeometry_.emplace(localFunction().localContext().geometry());

    return *hostGeometry_;
  }

private:
  /// Reference of the geometry
  ReferenceElement refElement_ = {};

  /// local parametrization of the element
  std::optional<LocalFunction> localFunction_;

  /// transformation of local coordinates to element-local coordinates
  LocalGeometry localGeometry_;

  // some data optionally provided
  mutable std::optional<DerivativeLocalFunction> derivativeLocalFunction_;
  mutable std::optional<FlatGeometry> flatGeometry_;
  mutable std::optional<HostGeometry> hostGeometry_;
  mutable std::vector<GlobalCoordinate> nCoefficients_;
  mutable std::vector<GlobalCoordinate> nGradients_;
};

// deduction guides
template <class RefGeo, class LF, class LG>
LocalFunctionGeometry (RefGeo, const LF&, const LG&)
  -> LocalFunctionGeometry<LF,LG>;

template <class I, class LF>
LocalFunctionGeometry (Geo::ReferenceElement<I>, const LF&)
  -> LocalFunctionGeometry<LF,DefaultLocalGeometry<typename I::ctype, I::dimension>,
                           LocalFunctionGeometryTraits<typename I::ctype> >;

// typedef for geometries on grid elements with local geometry is identity
template <class LocalFunction, class ctype, int dim>
using ElementLocalFunctionGeometry = LocalFunctionGeometry<LocalFunction,
  DefaultLocalGeometry<ctype,dim>, LocalFunctionGeometryTraits<ctype> >;

} // end namespace Dune

#endif // DUNE_CURVEDGEOMETRY_LOCALFUNCTIONGEOMETRY_HH
