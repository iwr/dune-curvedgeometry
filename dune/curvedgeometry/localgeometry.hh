#ifndef DUNE_CURVEDGEOMETRY_LOCALGEOMETRY_HH
#define DUNE_CURVEDGEOMETRY_LOCALGEOMETRY_HH

#include <dune/common/fmatrix.hh>

namespace Dune {
namespace Impl {

// Placeholder type for a trivial identity matrix without any functionality
struct IdentityMatrix {};

// multiplication of an identity-matrix with any real matrix
template <class K, int n, int m>
const FieldMatrix<K,n,m>& AB(const IdentityMatrix& A, const FieldMatrix<K,n,m>& B)
{
  return B;
}

} // end namespace Impl


// efficient implementation of a trivial local geometry for elements
template <class ctype, int mydim, int cdim = mydim>
struct DefaultLocalGeometry
{
  using LocalCoordinate = FieldVector<ctype,mydim>;
  using GlobalCoordinate = FieldVector<ctype,cdim>;

  template <class LocalCoordinate>
  decltype(auto) global(LocalCoordinate&& local) const
  {
    return std::forward<LocalCoordinate>(local);
  }

  template <class LocalCoordinate>
  Impl::IdentityMatrix jacobianTransposed(LocalCoordinate&& local) const
  {
    return {};
  }
};

} // end namespace Dune

#endif // DUNE_CURVEDGEOMETRY_LOCALGEOMETRY_HH
