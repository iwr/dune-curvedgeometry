#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <cmath>
#include <iostream>
#include <optional>
#include <type_traits>
#include <utility>

#include <dune/common/typeutilities.hh>
#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/curvedgeometry/localfunctiongeometry.hh>
#include <dune/curvedgeometry/localgeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/yaspgrid.hh>

const int order = 3;

#if USE_FLOAT128
  #include <dune/common/quadmath.hh>
  using ctype = Dune::Float128;
#else
  using ctype = double;
#endif


namespace Dune {

//! LocalFunction associated to the \ref AnalyticGridFunction
template <class LocalContext, class F>
class LocalAnalyticGridFunction;

//! Generator for \ref LocalAnalyticGridFunction
template <class LocalContext, class FF>
auto localAnalyticGridFunction (FF&& ff)
{
  using F = std::decay_t<FF>;
  return LocalAnalyticGridFunction<LocalContext, F>{std::forward<FF>(ff)};
}

template <class LC, class Functor>
class LocalAnalyticGridFunction
{
public:
  using LocalContext = LC;
  using Geometry = typename LocalContext::Geometry;

  using Domain = typename Geometry::GlobalCoordinate;
  using LocalDomain = typename Geometry::LocalCoordinate;
  using Range = std::result_of_t<Functor(Domain)>;
  using Signature = Range(LocalDomain);

public:
  //! Constructor. Stores the functor f by value
  template <class FF,
    disableCopyMove<LocalAnalyticGridFunction, FF> = 0>
  LocalAnalyticGridFunction (FF&& f)
    : f_(std::forward<FF>(f))
  {}

  //! bind the LocalFunction to the local context
  void bind (const LocalContext& localContext)
  {
    localContext_.emplace(localContext);
    geometry_.emplace(localContext.geometry());
  }

  //! unbind the localContext from the localFunction
  void unbind ()
  {
    geometry_.reset();
    localContext_.reset();
  }

  //! evaluate the stored function in local coordinates
  Range operator() (const LocalDomain& x) const
  {
    assert(!!geometry_);
    return f_(geometry_->global(x));
  }

  //! return the bound localContext.
  const LocalContext& localContext () const
  {
    assert(!!localContext_);
    return *localContext_;
  }

  //! obtain the functor
  const Functor& f () const
  {
    return f_;
  }

private:
  Functor f_;

  // some caches
  std::optional<LocalContext> localContext_;
  std::optional<Geometry> geometry_;
};

//! Derivative of a \ref LocalAnalyticGridFunction
template <class LC, class F>
auto derivative (const LocalAnalyticGridFunction<LC,F>& t)
  -> decltype(localAnalyticGridFunction<LC>(derivative(t.f())))
{
  return localAnalyticGridFunction<LC>(derivative(t.f()));
}

} // end namespace Dune


struct Projection
{
  Dune::FieldVector<ctype,3> operator() (const Dune::FieldVector<ctype,2>& x) const
  {
    using std::sin; using std::cos;
    return { x[0], x[1], sin(x[0])*cos(x[1]) };
  }

  friend auto derivative(Projection)
  {
    return [](const Dune::FieldVector<ctype,2>& x) {
      using std::sin; using std::cos;
      return Dune::FieldMatrix<ctype,3,2>{
        {1, 0},
        {0, 1},
        {cos(x[0])*cos(x[1]), -sin(x[0])*sin(x[1])}
      };
    };
  }
};

template <class Geometry>
void check(Geometry const& geo)
{
  // some basic checks of the constructability and assignability of geometries
  static_assert(std::is_copy_constructible_v<Geometry>);
  static_assert(std::is_copy_assignable_v<Geometry>);
  static_assert(std::is_move_constructible_v<Geometry>);
  static_assert(std::is_move_assignable_v<Geometry>);
}


int main(int argc, char** argv)
{
  using namespace Dune;
  MPIHelper& helper = MPIHelper::instance(argc, argv);

  using Grid = YaspGrid<2,EquidistantCoordinates<ctype,2>>;
  Grid grid({ctype(8.0),ctype(8.0)}, {32,32});

  using Element = typename Grid::template Codim<0>::Entity;
  using LocalFunction = LocalAnalyticGridFunction<Element,Projection>;
  using LocalGeometry = DefaultLocalGeometry<ctype,2,2>;
  using Geometry = LocalFunctionGeometry<LocalFunction,LocalGeometry>;

  FieldMatrix<ctype,2,2> I{{1,0},{0,1}};

  TestSuite test("curved geometry");
  LocalFunction localFunction(Projection{});
  LocalGeometry localGeometry{};
  for (const auto& e : elements(grid.leafGridView()))
  {
    localFunction.bind(e);

    // create a curved geometry
    LocalFunctionGeometry geometry(e.type(), localFunction, localGeometry);
    check(geometry);

    // test deduction guide
    LocalFunctionGeometry geometry2(referenceElement(e), localFunction);
    check(geometry2);

    // test additional typedef
    ElementLocalFunctionGeometry<LocalFunction,ctype,2> geometry3(referenceElement(e), localFunction);
    check(geometry3);

    const auto& quadRule = QuadratureRules<ctype,2>::rule(e.type(), 4);
    for (const auto& qp : quadRule) {
      auto Jt = geometry.jacobianTransposed(qp.position());
      auto Jtinv = geometry.jacobianInverseTransposed(qp.position());

      FieldMatrix<ctype,2,2> res;
      FMatrixHelp::multMatrix(Jt, Jtinv, res);
      res -= I;

      using std::sqrt;
      test.check(res.frobenius_norm() < sqrt(std::numeric_limits<ctype>::epsilon()),
                 "J^-1 * J == I");
    }
  }

  return test.report() ? 0 : 1;
}
