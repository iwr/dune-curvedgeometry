// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <cmath>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/curvedgeometry/curvedgeometry.hh>
#include <dune/curvedgeometry/parametrizedgeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/yaspgrid.hh>

const int order = 3;

using namespace Dune;
using LocalCoordinate  = FieldVector<double,2>;
using GlobalCoordinate = FieldVector<double,2>;
using WorldCoordinate  = FieldVector<double,3>;

template <class Geometry>
void check(Geometry const& /*geo*/)
{
  // some basic checks of the constructability and assignability of geometries
  static_assert(std::is_copy_constructible_v<Geometry>);
  static_assert(std::is_copy_assignable_v<Geometry>);
  static_assert(std::is_move_constructible_v<Geometry>);
  static_assert(std::is_move_assignable_v<Geometry>);
}

int main(int argc, char** argv)
{
  MPIHelper& helper = MPIHelper::instance(argc, argv);

  YaspGrid<2> grid({8.0,8.0}, {32,32});
  auto gridView = grid.leafGridView();
  const auto& indexSet = gridView.indexSet();

  FieldMatrix<double,2,2> I{{1,0},{0,1}};

  // coordinate projection
  auto project = [](const GlobalCoordinate& global) -> WorldCoordinate
  {
    return { global[0],
             global[1],
             std::sin(global[0])*std::cos(global[1]) };
  };

  TestSuite test("curved geometry");

  // 1. fill a global parametrization storage
  using Geometry = LagrangeCurvedGeometry<double, 2, 3, order>;
  using LocalFECache = typename Geometry::Traits::LocalFiniteElementCache;
  LocalFECache localFECache;

  std::vector<std::vector<WorldCoordinate>> parametrization(gridView.size(0));
  for (const auto& e : elements(gridView))
  {
    // projection from local coordinates
    auto X = [project,geo=e.geometry()](const LocalCoordinate& local) -> WorldCoordinate
    {
      return project(geo.global(local));
    };

    auto& localParametrization = parametrization[indexSet.index(e)];
    const auto& localFE = localFECache.get(e.type());

    // store coefficients of local parametrization in global storage
    localFE.localInterpolation().interpolate(X, localParametrization);
  }


  // 2. use the stored parametrization to construct a geometry
  for (const auto& e : elements(grid.leafGridView()))
  {
    const auto& quadRule = QuadratureRules<double,2>::rule(e.type(), 4);

    // create a curved geometry
    Geometry geometry1(e.type(), parametrization[indexSet.index(e)]);
    check(geometry1);
    for (const auto& qp : quadRule) {
      auto Jt = geometry1.jacobianTransposed(qp.position());
      auto Jtinv = geometry1.jacobianInverseTransposed(qp.position());

      FieldMatrix<double, 2, 2> res;
      FMatrixHelp::multMatrix(Jt, Jtinv, res);
      res -= I;
      test.check(res.frobenius_norm() < std::sqrt(std::numeric_limits<double>::epsilon()),
                 "J^-1 * J == I");
    }

    ParametrizedGeometry geometry2(e.type(), localFECache.get(e.type()), parametrization[indexSet.index(e)]);
    check(geometry2);
    for (const auto& qp : quadRule) {
      auto Jt = geometry2.jacobianTransposed(qp.position());
      auto Jtinv = geometry2.jacobianInverseTransposed(qp.position());

      FieldMatrix<double, 2, 2> res;
      FMatrixHelp::multMatrix(Jt, Jtinv, res);
      res -= I;
      test.check(res.frobenius_norm() < std::sqrt(std::numeric_limits<double>::epsilon()),
                 "J^-1 * J == I");
    }
  }

  return test.report() ? 0 : 1;
}
