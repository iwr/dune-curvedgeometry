// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <cmath>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/curvedgeometry/curvedgeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/yaspgrid.hh>

const int order = 3;

#if USE_FLOAT128
  #include <dune/common/quadmath.hh>
  using ctype = Dune::Float128;
#else
  using ctype = double;
#endif

template <class Geometry>
void check(Geometry const& /*geo*/)
{
  // some basic checks of the constructability and assignability of geometries
  static_assert(std::is_copy_constructible_v<Geometry>);
  static_assert(std::is_copy_assignable_v<Geometry>);
  static_assert(std::is_move_constructible_v<Geometry>);
  static_assert(std::is_move_assignable_v<Geometry>);
}

int main(int argc, char** argv)
{
  using namespace Dune;
  MPIHelper& helper = MPIHelper::instance(argc, argv);

  using Grid = YaspGrid<2,EquidistantCoordinates<ctype,2>>;
  Grid grid({ctype(8.0),ctype(8.0)}, {32,32});
  using LocalCoordinate  = FieldVector<ctype,2>;
  using GlobalCoordinate = FieldVector<ctype,2>;
  using WorldCoordinate  = FieldVector<ctype,3>;

  using Geometry = LagrangeCurvedGeometry<ctype, 2, 3, order>;

  FieldMatrix<ctype,2,2> I{{1,0},{0,1}};

  // coordinate projection
  auto project = [](const GlobalCoordinate& global) -> WorldCoordinate
  {
    using std::sin; using std::cos;
    return { global[0],
             global[1],
             sin(global[0])*cos(global[1]) };
  };

  TestSuite test("curved geometry");

  for (const auto& e : elements(grid.leafGridView()))
  {
    // projection from local coordinates
    auto X = [project,geo=e.geometry()](const LocalCoordinate& local) -> WorldCoordinate
    {
      return project(geo.global(local));
    };

    // create a curved geometry
    Geometry geometry(e.type(), X);
    check(geometry);

    const auto& quadRule = QuadratureRules<ctype,2>::rule(e.type(), 4);
    for (const auto& qp : quadRule) {
      auto Jt = geometry.jacobianTransposed(qp.position());
      auto Jtinv = geometry.jacobianInverseTransposed(qp.position());

      FieldMatrix<ctype,2,2> res;
      FMatrixHelp::multMatrix(Jt, Jtinv, res);
      res -= I;

      using std::sqrt;
      test.check(res.frobenius_norm() < sqrt(std::numeric_limits<ctype>::epsilon()),
                 "J^-1 * J == I");
    }
  }

  return test.report() ? 0 : 1;
}
