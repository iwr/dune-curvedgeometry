// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:
#ifdef HAVE_CONFIG_H
  #include "config.h"
#endif

#include <cmath>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/curvedgeometry/parametrizedgeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/localfunctions/lagrange/lagrangelfecache.hh>
#include <dune/localfunctions/lagrange/lfecache.hh>

const int order = 3;

#if USE_FLOAT128
  #include <dune/common/quadmath.hh>
  using ctype = Dune::Float128;
#else
  using ctype = double;
#endif

template <class Geometry>
void check(Geometry const& /*geo*/)
{
  // some basic checks of the constructability and assignability of geometries
  static_assert(std::is_copy_constructible_v<Geometry>);
  static_assert(std::is_copy_assignable_v<Geometry>);
  static_assert(std::is_move_constructible_v<Geometry>);
  static_assert(std::is_move_assignable_v<Geometry>);
}

int main(int argc, char** argv)
{
  using namespace Dune;
  MPIHelper& helper = MPIHelper::instance(argc, argv);

  using Grid = YaspGrid<2,EquidistantCoordinates<ctype,2>>;
  Grid grid({ctype(8.0),ctype(8.0)}, {32,32});
  using LocalCoordinate  = FieldVector<ctype,2>;
  using GlobalCoordinate = FieldVector<ctype,2>;
  using WorldCoordinate  = FieldVector<ctype,3>;

  FieldMatrix<ctype,2,2> I{{1,0},{0,1}};

  // coordinate projection
  auto project = [](const GlobalCoordinate& global) -> WorldCoordinate
  {
    using std::sin; using std::cos;
    return { global[0],
             global[1],
             sin(global[0])*cos(global[1]) };
  };

  TestSuite test("curved geometry");

  LagrangeLocalFiniteElementCache<ctype,ctype,2,order> lfeCache1;
#if ! USE_FLOAT128
  LagrangeLFECache<ctype,ctype,2> lfeCache2(order);
#endif
  for (const auto& e : elements(grid.leafGridView()))
  {
    // projection from local coordinates
    auto X = [project,geo=e.geometry()](const LocalCoordinate& local) -> WorldCoordinate
    {
      return project(geo.global(local));
    };

    const auto& quadRule = QuadratureRules<ctype,2>::rule(e.type(), 4);

    // create a curved geometry based on compiletime-order Lagrange Finite-Element
    ParametrizedGeometry geometry1(e.type(), lfeCache1.get(e.type()), X);
    check(geometry1);
    for (const auto& qp : quadRule) {
      auto Jt = geometry1.jacobianTransposed(qp.position());
      auto Jtinv = geometry1.jacobianInverseTransposed(qp.position());

      FieldMatrix<ctype,2,2> res;
      FMatrixHelp::multMatrix(Jt, Jtinv, res);
      res -= I;

      using std::sqrt;
      test.check(res.frobenius_norm() < sqrt(std::numeric_limits<ctype>::epsilon()),
                 "J^-1 * J == I");
    }

#if ! USE_FLOAT128
    // create a curved geometry based on runtime-order Lagrange Finite-Element
    ParametrizedGeometry geometry2(e.type(), lfeCache2.get(e.type()), X);
    check(geometry2);
    for (const auto& qp : quadRule) {
      auto Jt = geometry2.jacobianTransposed(qp.position());
      auto Jtinv = geometry2.jacobianInverseTransposed(qp.position());

      FieldMatrix<ctype,2,2> res;
      FMatrixHelp::multMatrix(Jt, Jtinv, res);
      res -= I;

      using std::sqrt;
      test.check(res.frobenius_norm() < sqrt(std::numeric_limits<ctype>::epsilon()),
                 "J^-1 * J == I");
    }
#endif
  }

  return test.report() ? 0 : 1;
}
