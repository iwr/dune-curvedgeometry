// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <cmath>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/curvedgeometry/curvedgeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/curvedgeometrydatacollector.hh>

const int order = 3;

using namespace Dune;
using LocalCoordinate  = FieldVector<double,2>;
using GlobalCoordinate = FieldVector<double,2>;
using WorldCoordinate  = FieldVector<double,2>;

int main(int argc, char** argv)
{
  MPIHelper::instance(argc, argv);

  using Grid = YaspGrid<2,EquidistantOffsetCoordinates<double,2>>;
  Grid grid({-1.0,-1.0}, {1.0,1.0}, {20,20});
  using GridView = typename Grid::LeafGridView;
  GridView gridView = grid.leafGridView();

  // coordinate projection
  auto project = [](const GlobalCoordinate& global) -> WorldCoordinate
  {
    return global * (std::sqrt(2.0)/global.two_norm());
  };

  std::vector<std::vector<WorldCoordinate>> parametrization(gridView.size(0));

  using Geometry = LagrangeCurvedGeometry<double, 2, 2, order>;
  using LocalFECache = typename Geometry::Traits::LocalFiniteElementCache;
  LocalFECache localFECache;
  const auto& indexSet = gridView.indexSet();
  for (const auto& e : elements(grid.leafGridView()))
  {
    // projection from local coordinates
    auto id = [geo=e.geometry()](const LocalCoordinate& local) -> WorldCoordinate
    {
      return geo.global(local);
    };

    const auto& localFE = localFECache.get(e.type());
    auto re = Dune::referenceElement<double,2>(e.type());

    auto& localParametrization = parametrization[indexSet.index(e)];
    localFE.localInterpolation().interpolate(id, localParametrization);
    if (e.hasBoundaryIntersections())
    {
      std::vector<std::size_t> containedDOFs;

      const auto& localCoeff = localFE.localCoefficients();
      std::size_t localSize = localCoeff.size();
      for (const auto& is : intersections(gridView, e)) {
        if (!is.boundary())
          continue;

        // collect all DOFs on the boundary
        std::size_t subEntityIndex = is.indexInInside();
        std::size_t subEntityCodim = 1;
        for (std::size_t i = 0; i < localSize; ++i)
        {
          auto localKey = localCoeff.localKey(i);
          if (re.subEntities(subEntityIndex, subEntityCodim, localKey.codim()).contains(localKey.subEntity()))
          {
            containedDOFs.push_back(i);
          }
        }
      }

      // project only boundary DOFs
      for (std::size_t i : containedDOFs)
        localParametrization[i] = project(localParametrization[i]);
    }
  }

  using DataCollector = Vtk::CurvedGeometryDataCollector<GridView,Geometry>;
  DataCollector dataCollector(gridView, parametrization);

  using Writer = Vtk::UnstructuredGridWriter<GridView,DataCollector>;
  Writer writer(stackobject_to_shared_ptr(dataCollector));

  writer.write("boundaryprojection.vtu");
}
