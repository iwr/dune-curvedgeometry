// -*- tab-width: 4; indent-tabs-mode: nil; c-basic-offset: 2 -*-
// vi: set et ts=4 sw=2 sts=2:

#ifdef HAVE_CONFIG_H
# include "config.h"
#endif

#include <cmath>
#include <iostream>

#include <dune/common/parallel/mpihelper.hh>
#include <dune/common/test/testsuite.hh>
#include <dune/curvedgeometry/curvedgeometry.hh>
#include <dune/geometry/quadraturerules.hh>
#include <dune/grid/yaspgrid.hh>
#include <dune/vtk/vtkwriter.hh>
#include <dune/vtk/datacollectors/curvedgeometrydatacollector.hh>

const int order = 3;

int main(int argc, char** argv)
{
  using namespace Dune;
  MPIHelper::instance(argc, argv);

  using Grid = YaspGrid<2>;
  Grid grid({8.0,8.0}, {32,32});
  using GridView = typename Grid::LeafGridView;
  GridView gridView = grid.leafGridView();

  using Geometry = LagrangeCurvedGeometry<double, 2, 3, order>;

  // coordinate projection
  using GlobalCoordinate = typename Grid::Codim<0>::Entity::Geometry::GlobalCoordinate;
  using WorldCoordinate  = typename Geometry::GlobalCoordinate;
  auto project = [](const GlobalCoordinate& global) -> WorldCoordinate
  {
    return { global[0],
             global[1],
             std::sin(global[0])*std::cos(global[1]) };
  };

  using DataCollector = Vtk::CurvedGeometryDataCollector<GridView,Geometry>;
  DataCollector dataCollector(gridView, project);

  using Writer = Vtk::UnstructuredGridWriter<GridView,DataCollector>;
  Writer writer(stackobject_to_shared_ptr(dataCollector));

  writer.write("curvedgeometry.vtu");
}
