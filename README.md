CurvedGeometry
==============
The `CurvedGeometry` is a Geometry parametrized by a local-to-global mapping that
is interpolated into a local basis. In the Traits type the local basis in encoded
by a LocalFiniteElement Cache type, that allows to construct a local FiniteElement
and with this also the local basis.

```c++
// <dune/curvedgeometry/curvedgeometry.hh>

template <class ctype, int mydim, int corddim, class Traits>
class CurvedGeometry;
```
The following type requirements must be fulfilled:
- `ctype`: Type of the coordiante vector components, i.e., representation for R. This type should
  be an *Arithmetic* type.
- `mydim`: Dimension of the entity-coordinate system this geometry is bound to.
- `coorddim`: Dimension of the global coordinates this geometry maps into.

There is a specialization of that type for Lagrange basis functions:
```c++
template <class ctype, int mydim, int corddim, int order>
using LagrangeCurvedGeometry = CurvedGeometry<ctype,mydim,corddim,
  CurvedGeometryTraits<ctype, LagrangeLocalFiniteElementCache<ctype,ctype,mydim,order>> >;
```
with
- `order`: Polynomial order of the lagrange basis functions. Should be > 0.

Example
-------
```c++
// construct a reference grid
Grid grid(...);

// define the geometry type
const int order = 3;
using Geometry = LagrangeCurvedGeometry<double, Grid::dimension, 3, order>;
using LocalCoordinate = typename Geometry::LocalCoordinate;
using GlobalCoordinate = typename Geometry::GlobalCoordinate;

for (const auto& e : elements(grid.leafGridView()))
{
  // sphere projection from local to global coordinates
  auto X = [geo=e.geometry()](const LocalCoordinate& local) -> GlobalCoordinate
  {
    auto x = geo.global(local);
    return x / x.two_norm();
  };

  // create a curved geometry
  Geometry geometry(e.type(), X);
}
```

LocalFunctionGeometry
=====================
The `LocalFunctionGeometry` is a Geometry parametrized by a local-function bound to an element.
The local-function is evaluated in the coordinates provided by a local-geometry mapping that
allows to construct also geometries of intersections.

```c++
// <dune/curvedgeometry/localfunctiongeometry.hh>

template <class LocalFunction, class LocalGeometry, class Traits>
class LocalFunctionGeometry;
```
The following type requirements must be fulfilled:
- `LocalGeometry` represents a geometric mapping from an entity of codim `c` to the element
  with codim `0`.
- `LocalFunction` represents a differentiable mapping with from the global coordinates of the
  `LocalGeometry` to the global coordinate of the target geometry. It must be bound to an element
  and must provide a free function `derivative(LocalFunction)` that creates the jacobian mapping
  of the coordinate mapping.

A sepcial form of `LocalFunctionGeometry` could be defined that lives in elements (codim = 0) of
a host grid:

```c++
template <class LocalFunction, class ctype, int mydim>
using ElementLocalFunctionGeometry = LocalFunctionGeometry<LocalFunction,
  DefaultLocalGeometry<ctype,mydim,mydim>, LocalFunctionGeometryTraits<ctype> >;
```

Example
-------
```c++
// construct a reference grid
Grid grid(...);

// sphere-mapping grid function
auto sphereGridFct = analyticGridFunction<Grid>([](const auto& x) { return x / x.two_norm(); });
auto sphereLocalFct = localFunction(sphereGridFct);

// define the geometry type
using Geometry = ElementLocalFunctionGeometry<decltype(sphereLocalFct), double, Grid::dimension>;
using LocalCoordinate = typename Geometry::LocalCoordinate;
using GlobalCoordinate = typename Geometry::GlobalCoordinate;

for (const auto& e : elements(grid.leafGridView()))
{
  sphereLocalFct.bind(e);

  // create a curved geometry
  Geometry geometry(e.type(), sphereLocalFct);

  sphereLocalFct.unbind();
}
```

Mirror
======
This repositories is mirrored to https://gitlab.com/iwr/dune-curvedgeometry.

Installation Instructions
=========================
`dune-curvedgeometry` requires the DUNE core modules, version 2.9 or later.
Please see the [general instructions for building DUNE modules](https://www.dune-project.org/doc/installation)
for detailed instructions on how to build the module.
